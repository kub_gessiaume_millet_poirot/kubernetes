Nous avons voulu faire un site web grace à nginx et associé à une base de données mysql.
Nous n'avons malheureusement pas pu tester le déploiment suite à une erreur que nous avons rencontré avec minikube et que nous n'avons pas réussi a dépanner 

Les commandes à taper:
- kubectl create namespace Notre_Site
- kubectl apply -f secret.yaml Notre_Site
- kubectl apply -f mysql-deploy.yaml Notre_Site
- kubectl apply -f mysql-service.yaml Notre_Site
- kubectl apply -f mysql-volume.yaml Notre_Site
- kubectl apply -f website-deploy.yaml Notre_Site
- kubectl apply -f website-service.yaml Notre_Site
- kubectl apply -f website-volume.yaml Notre_Site